class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :jira_username
      t.string :jira_password

      t.timestamps null: false
    end
  end
end
