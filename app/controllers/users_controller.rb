require "base64"
require 'httparty'

class UsersController < ApplicationController

	def login_jira
    end
	def jira_credentials
		@user = User.find_or_create_by(user_params)
    	enc   = Base64.encode64("#{@user.jira_username}:#{@user.jira_password}")
        header = {"Authorization" => "Basic #{enc}","Content-Type"=> "application/json"} 
        response = HTTParty.get("http://172.16.0.166/rest/api/2/user?username=#{@user.jira_username}", :headers => header )
        @detail = response.parsed_response
	end

	def show_user_details
	end



	private 
	def user_params
		p params
		params.require(:user).permit(:jira_username, :jira_password)
	end
end
